import React from 'react';
import { MetaSchemaTitle } from './MetaSchemaTitle';
import logoImg from 'assets/imgs/logos/api_spec.png';
import SwaggerUI from 'swagger-ui-react';
import "../../../utils/swagger-ui.css";
import property from '../../../configs/property.json';
const server = property.designerServerHost;

export function MetaDocs(props) {
  return (
    <div>
      <MetaSchemaTitle img={logoImg} title='활용 명세서' />
      <SwaggerUI url={`${server}/metas/${props.meta.id}/api-docs`}/>
    </div>
  )
}